import BodyParser from 'body-parser';
import cors from 'cors';
import jwt from 'express-jwt';
import morgan from 'morgan';
import helmet from 'helmet';

import { AppConfig, JwtConfig } from './../config';

export function initMiddleware(app) {
    // Enable cors
    app.use(cors());

    //Helmet
    app.use(helmet());

    // Parser data request
    app.use(BodyParser.json());
    app.use(BodyParser.urlencoded({
        extended: true
    }));

    //Write log request
    app.use(morgan('combined', AppConfig.logs));

    // Parser jwt
    app.use(jwt({
        secret: JwtConfig.secretKey,
        credentialsRequired: false,
        requestProperty: 'auth',
        getToken: (req) => {
            if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
                req.accessToken = req.headers.authorization.split(' ')[1];
                return req.headers.authorization.split(' ')[1];
            } else if (req.query && req.query.access_token) {
                req.accessToken = req.query.access_token;
                return req.query.access_token;
            }
            return false;
        }
    }));

    app.use((err, req, res, next) => {
        if (err.name === 'UnauthorizedError') {
            next();
            // res.status(401).send({
            //     message: 'Invalid token...'
            // });
        }

        if (err.code === 'permission_denied') {
            res.status(401).send('insufficient permissions');
        }
    });
}