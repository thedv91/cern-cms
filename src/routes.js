import graphqlHTTP from 'express-graphql';

import * as routes from './modules/post/routes';
import ChatRoutes from './modules/chat/routes';
import schema from './schema';


export function init(app) {

    app.use('/graphql', graphqlHTTP(req => {

        const startTime = Date.now();
        return {
            schema: schema.getSchema(),
            graphiql: true,
            pretty: true,
            extensions({ document, variables, operationName, result }) {
                return { runTime: Date.now() - startTime };
            }
        };
    }));

    app.use('/api/v1', routes.PostRouter);
    app.use('/api/v1', ChatRoutes(app));
}