/* @flow */
import graphqlHTTP from 'express-graphql';
import { GraphQLSchema, GraphQLObjectType } from 'graphql';
import Express, { Router } from 'express';

import GroupCtr from './../controllers/group.controller';
import { ChatQueries } from './../graphql';

export default function (app: any, route: Router) {
    route.route('/chat/group')
        .get(GroupCtr.list)
        .post(GroupCtr.create);

    route.route('/chat/group/:groupId')
        .get(GroupCtr.read)
        .delete(GroupCtr.remove);

    route.route('/chat/group/:groupId/chat')
        .post(GroupCtr.talk);

    route.use('/chat/graphql', graphqlHTTP(req => {

        let rootQuery = new GraphQLObjectType({
            name: 'Query',
            fields: () => (ChatQueries)
        });
        const schema = new GraphQLSchema({
            query: rootQuery
        });
        const startTime = Date.now();
        return {
            schema: schema,
            graphiql: true,
            pretty: true,
            extensions({ document, variables, operationName, result }) {
                return { runTime: Date.now() - startTime };
            }
        };
    }));

    route.param('groupId', GroupCtr.getById);
}