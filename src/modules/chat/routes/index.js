/* @flow */
import Express, { Router } from 'express';
import GroupRoute from './group.route';

let router = Router();
export default function (app: Express) {
    GroupRoute(app, router);

    return router;
}