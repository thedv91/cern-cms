import group from './group';
import message from './message';

export default Object.assign({}, group, message);