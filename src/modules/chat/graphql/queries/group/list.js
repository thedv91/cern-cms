import { GraphQLList } from 'graphql';
import ottoman from 'ottoman';

import groupType from './../../types/group';
import GroupModel from './../../../models/group.model';

let options = {
    limit: 10,
    skip: 0,
    consistency: ottoman.Consistency.LOCAL
};

export default {
    name: 'ChatGroupList',
    type: new GraphQLList(groupType),
    args: {

    },
    resolve(root, args) {
        return new Promise((resolve, reject) => {
            GroupModel.find({}, options, (err, datas) => {
                if (err)
                    return reject(err);
                return resolve(datas);
            });

        });
    }
};