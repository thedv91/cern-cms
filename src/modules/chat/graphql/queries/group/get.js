import { GraphQLID, GraphQLString } from 'graphql';
import _ from 'lodash';
import GroupType from './../../types/group';
import GroupModel from './../../../models/group.model';

export default {
    type: GroupType,
    description: 'Get Group by id',
    args: {
        id: {
            name: 'id',
            type: GraphQLString
        }
    },
    resolve: (root, args, context, { rootValue }) => {

        const id = args.id;
        return new Promise((resolve, reject) => {
            GroupModel.findById(id, (err, datas) => {
                if (err)
                    return reject(err);

                return resolve(_.first(datas));
            });
        });
    }
};