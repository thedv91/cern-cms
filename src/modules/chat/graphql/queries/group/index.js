import GetChatGroup from './get';
import ChatGroupList from './list';

export default {
    ChatGroupList,
    GetChatGroup
};