import { GraphQLID, GraphQLString } from 'graphql';
import MessageType from './../../types/message';
import MessageModel from './../../../models/message.model';

export default {
    type: MessageType,
    description: 'Get Message by group id',
    args: {
        id: {
            name: 'id',
            type: GraphQLString
        }
    },
    resolve: (root, args, context, { rootValue }) => {

        const id = args.id;
        return new Promise((resolve, reject) => {
            MessageModel.findByGroup(id, (err, datas) => {
                if (err)
                    return reject(err);

                return resolve(datas);
            });
        });
    }
};