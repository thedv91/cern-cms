import {
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLString,
    GraphQLList
} from 'graphql';

const groupType = new GraphQLObjectType({
    name: 'ChatGroup',
    fields: {
        _id: {
            type: new GraphQLNonNull(GraphQLString)
        },
        name: {
            type: GraphQLString
        }
    }
});

export default groupType;