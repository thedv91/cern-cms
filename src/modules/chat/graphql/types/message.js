import {
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLString,
    GraphQLList
} from 'graphql';

import groupType from './group';

const messageType = new GraphQLObjectType({
    name: 'ChatMessage',
    fields: {
        _id: {
            type: new GraphQLNonNull(GraphQLString)
        },
        message: {
            type: new GraphQLNonNull(GraphQLString)
        },
        createdAt: {
            type: GraphQLString
        },
        toGroup: {
            type: groupType
        }
    }
});

export default messageType;