import ottoman from 'ottoman';
import User from './../../user/models/user.model';
import Group from './group.model';

const Message = ottoman.model('ChatMessage', {
    message: 'string',
    createdAt: 'Date',
    toGroup: {
        ref: 'ChatGroup'
    },
    sendBy: {
        ref: 'User'
    }
}, {
        index: {
            findBySender: {
                by: 'sendBy'
            },
            findByGroup: {
                by: 'toGroup'
            }
        }
    });


export default Message;