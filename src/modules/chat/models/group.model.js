import ottoman from 'ottoman';
import User from './../../user/models/user.model';

const Group = ottoman.model('ChatGroup', {
    groupID: {
        type: 'string',
        auto: 'uuid',
        readonly: true
    },
    name: 'string',
    createdAt: {
        type: 'Date',
        default: Date.now
    },
    createBy: {
        ref: 'User'
    },
    members: [{
        ref: 'User'
    }]
}, {
        index: {
            findByGroupID: {
                type: 'refdoc',
                by: 'groupID'
            },
            findByName: {
                by: 'name',
            },
            findById: {
                by: '_id',
                type: 'refdoc'
            }
        }
    });


export default Group;