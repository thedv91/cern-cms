export default function (io, socket) {

    // console.log('connected', io.sockets.connected);

    io.emit('chatMessage', {
        type: 'status',
        text: 'Is now connected',
        created: Date.now()
    });

    // Send a chat messages to all connected sockets when a message is received
    socket.on('chatMessage', (message) => {
        message.type = 'message';
        message.created = Date.now();

        // Emit the 'chatMessage' event
        io.emit('chatMessage', message);
    });

    // Emit the status event when a socket client is disconnected
    socket.on('disconnect', () => {
        io.emit('chatMessage', {
            type: 'status',
            text: 'disconnected',
            created: Date.now()
        });
    });
}