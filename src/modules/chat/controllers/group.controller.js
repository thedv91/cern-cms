import _ from 'lodash';
import ottoman from 'ottoman';

import BaseController from './../../../lib/controller/base.controller';
import GroupModel from './../models/group.model';
import MessageModel from './../models/message.model';


let filters = {
    name: 'CC',
};

let options = {
    limit: 10,
    skip: 0,
    consistency: ottoman.Consistency.LOCAL
};


class GroupController extends BaseController {
    /**
     *
     * @api {get} /v1/chat/group Get list group chat
     * @apiName Chat list
     * @apiGroup Chat
     * @apiVersion  1.0.0
     *
     * @apiSuccess (200) {Array} name description
     *
     * @apiSuccessExample {Array} Success-Response:
       [{
           name : value
       },{
           ...
       }]
     *
     *
     */
    list(req, res) {
        GroupModel.find({}, options, (err, datas) => {
            if (err)
                return res.status(400).send(err);
            return res.json(datas);
        });
    }

    /**
     *
     * @api {post} /v1/chat/group Create chat group
     * @apiName Chat Group
     * @apiGroup Chat
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {String} name description
     *
     * @apiSuccess (200) {Object} name description
     *
     * @apiParamExample  {Object} Request-Example:
       {
           name : value
       }
     *
     *
     * @apiSuccessExample {Object} Success-Response:
       {
           name : value
       }
     *
     *
     */
    create(req, res) {
        const group = new GroupModel(req.body);
        group.save(error => {
            if (error)
                return res.status(400).send(error);
            return res.json(group);
        });
    }

    /**
     *
     * @api {delete} /v1/chat/group/:id Remove group
     * @apiName remove Group
     * @apiGroup Chat
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {String} id description
     *
     * @apiSuccess (200) {Object} name description
     *
     * @apiSuccessExample {Object} Success-Response:
       {
           name : value
       }
     *
     *
     */
    remove(req, res) {

    }

    /**
     *
     * @api {get} /v1/chat/group/:groupId Get group by id
     * @apiName Group
     * @apiGroup Chat
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {String} groupId description
     *
     * @apiSuccess (200) {Object} name description
     *
     * @apiSuccessExample {Object} Success-Response:
       {
           name : value
       }
     *
     *
     */
    read(req, res) {
        return res.json(req.group);
    }

    /**
     *
     * @api {post} /v1/chat/group/:id/chat Chat to group
     * @apiName Group chat
     * @apiGroup Chat
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {String} id description
     * @apiParam  {String} message Message
     *
     * @apiSuccess (200) {Object} name description
     *
     *
     * @apiSuccessExample {Object} Success-Response:
       {
           property : value
       }
     *
     *
     */
    talk(req, res) {

        let message = new MessageModel(req.body);
        message.toGroup = req.group;
        message.save(error => {
            if (error)
                return res.status(400).send(error);
            return res.json(message);
        });
    }

    getById(req, res, next, id) {
        GroupModel.findById(id, { load: ['members'] }, (err, obj) => {

            if (err)
                return res.status(400).send(err);
            if (obj.length >= 1) {
                req.group = _.first(obj);
            } else {
                req.group = obj;
            }

            return next();
        });
    }
}

export default new GroupController();