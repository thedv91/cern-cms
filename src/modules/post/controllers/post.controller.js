// @flow

import { Request, Response } from 'express';
import PostModel from './../models/post.model';
import { bucket } from './../../../config/db';
import couchbase, { N1qlQuery } from 'couchbase';
import BaseController from './../../../lib/controller/base.controller';


class PostController extends BaseController {

    constructor() {
        super();
    }


    /**
     *
     * @api {get} /v1/post Get list post
     * @apiName Get post list
     * @apiGroup Post
     * @apiVersion  1.0.0
     *
     * @apiSuccess (200) {Array} name description
     *
     * @apiSuccessExample {Array} Success-Response:
       [{
           _id: fa
           name : value
       },{
           ....
       }]
     *
     *
     */
    list(req: Request, res: Response) {

        const post = new PostModel(req.body);
        post.save(error => {
            if (error)
                return res.status(400).send(error);
            return res.json(post);
        });
    }

    /**
     *
     * @api {post} /v1/post Create post
     * @apiName Create Post
     * @apiGroup Post
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {String} name description
     *
     * @apiSuccess (200) {object} name description
     *
     * @apiParamExample  {object} Request-Example:
       {
           name : value
       }
     *
     *
     * @apiSuccessExample {type} Success-Response:
       {
           name : value
       }
     *
     *
     */
    create(req: Request, res: Response) {
        const post = new PostModel(req.body);
        post.save(error => {
            if (error)
                return res.status(400).send(error);
            return res.json(post);
        });
    }

    /**
     *
     * @api {get} /v1/post/:id Get post by ID
     * @apiName Post
     * @apiGroup Post
     * @apiVersion  1.0.0
     *
     *
     * @apiParam  {String} id
     *
     * @apiSuccess (200) {object} name description
     *
     * @apiSuccessExample {object} Success-Response:
       {
           name : value
       }
     *
     *
     */
    read(req: Request, res: Response) {
        return res.json(req.post);
    }

    getByID(req: Request, res: Response, next: Function, id: string) {
        PostModel.findByName(id, (err, obj) => {
            if (err)
                return res.status(400).send(err);
            req.post = obj;
            return next();
        });
    }
}

export default new PostController();