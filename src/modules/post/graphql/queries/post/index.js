import GetPost from './post';
import PostList from './list';

export default {
    GetPost,
    PostList
};