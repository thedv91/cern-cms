import { GraphQLList } from 'graphql';

import postType from './../../types/post';
import PostModel from './../../../models/post.model';

export default {
    name: 'PostList',
    type: new GraphQLList(postType),
    args: {

    },
    resolve(root, args) {
        return new Promise((resolve, reject) => {
            PostModel.find({}, (err, datas) => {
                if (err)
                    return reject(err);
                return resolve(datas);
            });

        });
    }
};