import { GraphQLID, GraphQLString } from 'graphql';
import _ from 'lodash';

import postType from './../../types/post';
import PostModel from './../../../models/post.model';

export default {
    type: postType,
    description: 'Get Post by name',
    args: {
        name: {
            name: 'name',
            type: GraphQLString
        }
    },
    resolve: (root, args, context, { rootValue }) => {

        const name = args.name;
        return new Promise((resolve, reject) => {
            PostModel.findByName(name, (err, datas) => {
                if (err)
                    return reject(err);

                return resolve(_.first(datas));
            });
        });
    }
};