import {
    GraphQLObjectType,
    GraphQLNonNull,
    GraphQLString
} from 'graphql';

const postType = new GraphQLObjectType({
    name: 'Post',
    fields: {
        _id: {
            type: new GraphQLNonNull(GraphQLString)
        },
        name: {
            type: GraphQLString
        }
    }
});

export default postType;