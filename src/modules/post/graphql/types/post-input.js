import {
    GraphQLInputObjectType,
    GraphQLString,
    GraphQLNonNull
} from 'graphql';

const postInputType = new GraphQLInputObjectType({
    name: 'PostInput',
    fields: {
        _id: { type: GraphQLString },
        name: { type: new GraphQLNonNull(GraphQLString) }
    }
});

export default postInputType;