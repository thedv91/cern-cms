import PostQueries from './queries';
import PostMutations from './mutations';

export {
    PostQueries,
    PostMutations
};