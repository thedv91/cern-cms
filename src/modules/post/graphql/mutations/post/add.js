import { GraphQLNonNull } from 'graphql';

import postType from './../../types/post';
import postInputType from '../../types/post-input';
import PostModel from './../../../models/post.model';


export default {
    type: postType,
    description: 'Add post.',
    args: {
        data: {
            name: 'data',
            type: new GraphQLNonNull(postInputType)
        }
    },
    resolve(root, args) {
        const { name } = args.data;
        const post = new PostModel({
            name
        });

        return new Promise((resolve, reject) => {
            post.save(error => {
                if (error)
                    return reject(error);
                return resolve(post);
            });
        });
    }
};