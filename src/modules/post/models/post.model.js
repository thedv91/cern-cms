import ottoman from 'ottoman';

const Post = ottoman.model('Post', {
    name: {
        type: 'string'
    }
}, {
        index: {
            findByName: {
                by: 'name',
                type: 'refdoc'
            }
        }
    });

Post.prototype.dance = function () {
    console.log('I am furniture, I do not dance.');
};

export default Post;