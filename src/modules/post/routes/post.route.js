import { Router } from 'express';
import PostCtr from './../controllers/post.controller';

const route = Router();

route.route('/post')
    .post(PostCtr.create);

route.route('/post/:postId')
    .get(PostCtr.read);

route.param('postId', PostCtr.getByID);

export default route;