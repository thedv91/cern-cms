import ottoman from 'ottoman';


const User = ottoman.model('User', {
    name: 'string',
    username: 'string',
    roles: [{
        type: 'string',
        permissions: ['string']
    }]
}, {
        index: {
            findByName: {
                by: 'name'
            },
            findByUserName: {
                by: 'username',
                type: 'refdoc'
            }
        }
    });

export default User;