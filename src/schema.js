import lodash from 'lodash';
import {
    GraphQLObjectType,
    GraphQLSchema
} from 'graphql';

import { PostQueries, PostMutations } from './modules/post/graphql';
import { ChatQueries } from './modules/chat/graphql';

class SchemaManager {

    constructor() {
        this.init();
    }

    init() {
        this.queryFields = lodash.clone(Object.assign({}, PostQueries, ChatQueries));
        this.mutationFields = lodash.clone(Object.assign({}, PostMutations));
        this.createRoot();
    }

    createRoot() {
        this.rootQuery = new GraphQLObjectType({
            name: 'Query',
            fields: () => (this.queryFields)
        });
        this.rootMutation = new GraphQLObjectType({
            name: 'Mutation',
            fields: () => (this.mutationFields)
        });
    }

    getSchema() {
        const schema = {
            query: this.rootQuery
        };

        if (Object.keys(this.mutationFields).length) {
            schema.mutation = this.rootMutation;
        }

        return new GraphQLSchema(schema);
    }
}

export default new SchemaManager();