import Express, { Router } from 'express';
import path from 'path';
import couchbase from 'couchbase';
import ottoman from 'ottoman';
import winston from 'winston';

import { initMiddleware } from './middleware/setup';
import socket from './config/socket.io';
import * as routes from './routes';


const app = Express();
const expressRouter = Express.Router();

app.use(Express.static('static'));

// Init middlwraew
initMiddleware(app);

// Init router
routes.init(app);

app.get('*', (req, res) => {
    return res.sendFile(path.join(process.cwd(), 'views/index.html'));
});


ottoman.ensureIndices((error) => {
    if (error) {
        winston.error('ERROR: %j', error);
    }
});

export function start() {
    const server = socket(app);
}
