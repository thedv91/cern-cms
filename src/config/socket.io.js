import socketio from 'socket.io';
import http from 'http';
import https from 'https';
import fs from 'fs';
import path from 'path';
import glob from 'glob';
import spdy from 'spdy';

import AppConfig from './app';

import PostSocket from './../modules/post/sockets';
import ChatSocket from './../modules/chat/sockets';

const options = {
    // Private key
    key: fs.readFileSync(path.join(process.cwd(), '/keys/spdy-key.pem')),

    // Fullchain file or cert file (prefer the former)
    cert: fs.readFileSync(path.join(process.cwd(), '/keys/spdy-cert.pem'))
};


export default function (app) {

    // const server = http.Server(app);
    // server.listen(AppConfig.PORT);

    const server = spdy.createServer(options, app);

    server.listen(AppConfig.PORT, error => {
        if (error) {
            console.log(error);
        }
    });

    // Create a new Socket.io server

    const io = socketio(server);

    // Add an event listener to the 'connection' event
    io.on('connection', (socket) => {
        // glob('./src/modules/*/sockets/**/*.js', (er, files) => {
        //     files.forEach((socketConfiguration, index) => {
        //         require(path.resolve(socketConfiguration))(io, socket);
        //     });
        // });
        PostSocket(io, socket);
        ChatSocket(io, socket);
    });

    return server;
}