import winston from 'winston';
import path from 'path';
import dateFormat from 'dateformat';

let accessLogStream;

switch (process.env.LOG_TYPE) {
    case 'daily':
        const name = dateFormat(Date.now(), 'yyyy-mm-dd');
        accessLogStream = path.join(process.cwd(), `/storage/logs/systems/${name}.log`);
        break;
    default:
        accessLogStream = path.join(process.cwd(), '/storage/logs/systems/access.log');
        break;
}

// winston.add(winston.transports.File, {
//     filename: accessLogStream
// });
// winston.remove(winston.transports.Console);


winston.configure({
    transports: [
        new (winston.transports.File)({ filename: accessLogStream })
    ]
});