import ottoman from 'ottoman';
import couchbase from 'couchbase';
import winston from 'winston';

let couchbaseConnected = false;

const cluster = new couchbase.Cluster(process.env.DB_HOST || 'http://localhost');
const bucket = cluster.openBucket(process.env.DB_NAME || 'mern-dev', process.env.DB_PASSWORD || '', err => {
    if (err) {
        winston.log('error', 'CONNECT ERROR: %j', err);
    }
});

bucket.on('error', (err) => {
    couchbaseConnected = false;
    winston.log('error', 'CONNECT ERROR: %j', err);
});

bucket.on('connect', () => {
    couchbaseConnected = true;
    winston.log('info', 'connected couchbase');
});

bucket.operationTimeout = 120 * 1000;


// ottoman.bucket = bucket;

ottoman.store = new ottoman.CbStoreAdapter(bucket, couchbase);

export {
    cluster,
    bucket
};