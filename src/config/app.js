import fs from 'fs';
import path from 'path';
import dateFormat from 'dateformat';

let accessLogStream;

switch (process.env.LOG_TYPE) {
    case 'daily':
        const name = dateFormat(Date.now(), 'yyyy-mm-dd');
        accessLogStream = fs.createWriteStream(path.join(process.cwd(), `/storage/logs/apps/${name}.log`), { flags: 'a' });
        break;
    default:
        accessLogStream = fs.createWriteStream(path.join(process.cwd(), '/storage/logs/apps/access.log'), { flags: 'a' });
        break;
}

export default {
    PORT: process.env.PORT || 8080,
    logs: {
        stream: accessLogStream
    }
};