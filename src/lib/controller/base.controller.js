import AuthController from './auth.controller';

class BaseController extends AuthController {
    constructor() {
        super();
    }

}

export default BaseController;