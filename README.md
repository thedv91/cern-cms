### CERN stack (Couchbase + Express + React + Node)
- A API framework with ES6 support [flowtype](https://flow.org/en/) and [ESLint](http://eslint.org/)
- Support GraphQL
- Support RESTful API
- Json web token

### TL;DR
```bash
yarn install
```

#### Start development
```bash
yarn start
```

#### Build production
```bash
yarn build
```
