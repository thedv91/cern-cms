FROM node:boron

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install pm2
RUN npm install pm2 -g

# Install app dependencies
COPY package.json yarn.lock /usr/src/app/
RUN yarn install --production

# Bundle app source
COPY build logs views server.js /usr/src/app/

EXPOSE 8080
CMD [ "pm2", "start", "server.js" ]